
# coding: utf-8

# In[1]:

import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
from sklearn.tree import DecisionTreeRegressor
from sklearn.cross_validation import train_test_split

def aggregate_bike_data(data_filenames):
    dfq = pd.read_csv(data_filenames[0], header=0,usecols=["Start date"], parse_dates=[0], infer_datetime_format=True)
    for fname in data_filenames:
        if fname != data_filenames[0]:
            dft = pd.read_csv(fname, header=0,usecols=["Start date"], parse_dates=[0], infer_datetime_format=True)
            dfq = pd.concat([dfq, dft]) 
    dfq['date'] = dfq['Start date'].apply(lambda x: dt.datetime.strftime(x,'%Y%m%d'))
    dfr = pd.DataFrame({'count' : dfq.groupby( ["date"] , as_index=False ).size()}).reset_index()
    dfr = dfr.set_index(['date'])
    return dfr

def integrate_weather_data(df, weather_filename):
    dfwtemp = pd.read_csv(weather_filename, header=0, parse_dates=['DATE'])
    dfwtemp['date2'] = dfwtemp['DATE'].apply(lambda x: dt.datetime.strftime(x,'%Y%m%d'))
    dfwtemp = dfwtemp.set_index(['DATE'])
    dfw = pd.merge(dfwtemp, df, right_index=True, left_index=True)
    return dfw

def create_month_plot(df):
    temp = pd.DatetimeIndex(df.index)
    df['month'] = temp.month
    df = df.groupby('month', as_index=False).sum()
    df['countsum'] = df.groupby('month')['count'].transform(sum)
    df.plot(x='month', y='countsum', kind='bar', title ="Number of bikeshares per month", figsize=(8,4),legend=True, fontsize=6, label='Bikeshares')

def create_scatterplot(df):
    qx = df['count'].values
    qy = df['TMAX'].values
    cmhot = plt.cm.get_cmap("hot")
    plt.scatter(qx, qy,  cmap=cmhot, marker='o', label='Monthly bikeshares')

def run_regression(df):
    qX = df.iloc[:,2:22]
    qy = df['count'].values.tolist()
    qX_train, qX_test, qy_train, qy_test = train_test_split(qX, qy, test_size=0.2)
    estimator = DecisionTreeRegressor()
    estimator.fit(qX_train, qy_train)
    predict = estimator.predict(qX_test)
    score = estimator.score(qX_test, qy_test)
    return score
    
def run():
    fnames = ["2014-Q1-Trips-History-Data2.csv",
              "2014-Q2-Trips-History-Data2.csv",
              "2014-Q3-Trips-History-Data3.csv",
              "2014-Q4-Trips-History-Data.csv"]
   
    df = aggregate_bike_data(fnames)
    print(df.head(10))
    wdf = integrate_weather_data(df, "dc-weather-2014.csv")
    print(wdf.head(10))
    create_month_plot(wdf)
    plt.show() # shows the plot on screen
    create_scatterplot(wdf)
    plt.show() # shows the plot on screen
    print("The plots show...Hot weather is key foctor, Max bikesharing, Compare bikeshare X winter: To 2X Spring, 2.5X Summer and 2X Fall") # ***Complete this sentence!***
    print("Score after running regression:", run_regression(wdf))
    print("The key attributes used by the predictor are: TMAX Max temp and Number of bikeshares per day") # ***Complete***
  
if __name__ == '__main__':
    run()


# In[ ]:



